<?php

use adapters\BaseAdapter;
use components\ConfigStorage;
use formatters\AbstractFormatDecorator;
use formatters\FormatterInterface;
use formatters\JsonFormatDecorator;
use formatters\XmlFormatDecorator;
use providers\ProductProvider;

/**
 * Class Client
 */
class Client
{
    /**
     * @var ConfigStorage
     */
    private $configStorage;

    /**
     * @var ProductProvider
     */
    private $productsProvider;

    public function __construct(ConfigStorage $configStorage, ProductProvider $productsProvider)
    {
        $this->configStorage = $configStorage;
        $this->productsProvider = $productsProvider;
    }


    /**
     * prints formatted line
     */
    public function print(){
        try {
            foreach ($this->getContentItems() as $contentItem) {
                $contentItem = $this->decorateWithFormatter($contentItem);
                echo $contentItem->format().PHP_EOL;
            }
        }
        catch( \Throwable $e) {
           //log error ;
            echo "Some error occured".PHP_EOL.$e->getMessage();
        }
    }


    /**
     * @return Generator|FormatterInterface[]
     */
    private function getContentItems()
    {
        foreach ($this->productsProvider->getProducts() as $product){
            yield BaseAdapter::getAdapter($product);
        }
    }

    /**
     * @param FormatterInterface $content
     * @return FormatterInterface
     * @throws Exception
     */
    private function decorateWithFormatter(FormatterInterface $content){
        $format = $this->configStorage->getFormat();
        return AbstractFormatDecorator::getDecorator($format, $content);
    }



}