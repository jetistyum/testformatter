<?php
namespace products;

/**
 * Class ProductA
 */
final class ProductA {

    /**
     * @return string
     */
    public function getCustomPublicDataFromA()
    {
        return 'ProductA data';
    }

}
