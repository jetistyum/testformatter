<?php
namespace products;

/**
 * Class ProductB
 */
final class ProductB {

    /**
     * @return string
     */
    public function getCommonPublicDataFromB()
    {
        return 'ProductB data';
    }

}
