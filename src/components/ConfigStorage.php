<?php
namespace components;
/**
 * Class ConfigStorage
 */
class ConfigStorage {

    private $config;

    /**
     * ConfigStorage constructor.
     * @param string $filename
     */
    function __construct(string $filename)
    {
        if (!file_exists($filename) || !is_readable($filename)){
            throw new \InvalidArgumentException('Can"t open file');
        }
        $this->config = require($filename);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getFormat():string {
        $availableFormats = ['xml', 'json'];
        $format  = $this->config['format'];
        if (!in_array($format, $availableFormats)){
            throw new \Exception('Invalid configuration of format');
        }
        return  $format;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    private function get($key, $default=null)
    {
        if (!array_key_exists($key, $this->config)){
            return $default;
        }
        return $this->config[$key];
    }


}