<?php


namespace formatters;


class XmlFormatDecorator extends AbstractFormatDecorator
{
    /**
     * @inheritDoc
     */
    public function format()
    {
        return '<data>'.$this->content->format().'</data>';
    }
}