<?php


namespace formatters;


class JsonFormatDecorator  extends AbstractFormatDecorator
{
    /**
     * @inheritDoc
     * @return string
     */
    public function format()
    {
        return json_encode(['data'=>$this->content->format()]);
    }
}