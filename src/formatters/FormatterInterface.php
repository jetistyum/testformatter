<?php

namespace formatters;

/**
 * Interface FormatterInterface
 */
interface FormatterInterface {

    /**
     * @return string
     */
    public function format();

}