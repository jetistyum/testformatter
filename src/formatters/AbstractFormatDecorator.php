<?php


namespace formatters;


abstract class AbstractFormatDecorator implements FormatterInterface
{
    /**
     * @var FormatterInterface
     */
    protected $content;

    /**
     * JsonFormatter constructor.
     * @param FormatterInterface $content
     */
    private function __construct(FormatterInterface $content)
    {
        $this->content = $content;
    }


    public static function getDecorator(string $format, FormatterInterface $content){
        switch ($format){
            case 'xml':
                return new XmlFormatDecorator($content);
            case 'json':
                return new JsonFormatDecorator($content);
            default:
                throw new \InvalidArgumentException('Invalid format provided');
        }
    }


}