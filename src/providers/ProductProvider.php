<?php


namespace providers;

use products\ProductA;
use products\ProductB;
use products\ProductC;

/**
 * Class ProductProvider
 * @package providers
 */
class ProductProvider
{

    /**
     * @return \Generator|ProductA[]|ProductB[]|ProductC[]
     */
    public function getProducts():\Generator
    {
        yield from [
            new ProductA(),
            new ProductB(),
            new ProductC(),
        ];
    }
}