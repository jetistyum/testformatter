<?php


namespace adapters;

use products\ProductA;

/**
 * Class AAdapter
 * @package adapters
 * @property ProductA $product
 */
class AAdapter extends BaseAdapter
{
    /**
     * @return string
     */
    public function format(): string
    {
        return  $this->product->getCustomPublicDataFromA();
    }
}