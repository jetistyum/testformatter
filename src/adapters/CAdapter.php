<?php


namespace adapters;

use products\ProductC;

/**
 * Class CAdapter
 * @package adapters
 * @property ProductC $product
 */
class CAdapter extends BaseAdapter
{
    /**
     * @return string
     */
    public function format(): string
    {
        return $this->product->getPrivateDataFromC();
    }
}