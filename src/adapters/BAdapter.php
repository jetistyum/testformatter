<?php


namespace adapters;

use products\ProductB;

/**
 * Class BAdapter
 * @package adapters
 *  * @property ProductB $product
 */
class BAdapter extends BaseAdapter
{
    /**
     * @return string
     */
    public function format(): string
    {
        return $this->product->getCommonPublicDataFromB();
    }
}