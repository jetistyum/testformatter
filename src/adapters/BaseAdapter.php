<?php


namespace adapters;



use formatters\FormatterInterface;
use products\ProductA;
use products\ProductB;
use products\ProductC;

/**
 * Abstract Class BaseAdapter
 * @package adapters
 * @property ProductB $product
 */
abstract class BaseAdapter implements FormatterInterface
{
    protected $product;

        private function __construct($product)
        {
            $this->product = $product;
        }

    /**
     * @param $instance
     * @return BaseAdapter
     */
        public static function getAdapter($instance){
            switch (true){
                case $instance instanceof ProductA:
                    return new AAdapter($instance);
                case $instance instanceof ProductB:
                    return new BAdapter($instance);
                case $instance instanceof ProductC:
                    return new CAdapter($instance);
                default:
                    throw new \InvalidArgumentException('Invalid product instance');
            }
        }


}