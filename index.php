<?php

use components\ConfigStorage;
use providers\ProductProvider;

require __DIR__ . '/vendor/autoload.php';

$configStorage = new ConfigStorage('config.php');
$productProvider = new ProductProvider();
$client = new Client($configStorage, $productProvider);
$client->print();